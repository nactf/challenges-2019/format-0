CC=gcc
CFLAGS=-std=c90 -m32 -fno-stack-protector -z execstack -no-pie -fno-pic -Wl,-rpath,.

.PHONY: default
default: format-0

%: %.c
	$(CC) -o $@ $< $(CFLAGS)
